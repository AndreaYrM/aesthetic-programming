[MiniX2.1 (smiley)](https://andreayrm.gitlab.io/aesthetic-programming/MiniX2.1/)      

[See code to MiniX2.1](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX2.1/sketch.js)


![Screenshot](MiniX2.1/Introvertemoji.png)

1.	A ‘smiley’ emoji who I like to call the introvert emoji, this particular emoji has headphones on and is in a more neutral mood. In addition, the emoji has a slide function for the purpose to change its colour from blue to more pinkish colour, the reason for this was for the emoji to be able to blend in with the background, like, in some cases, introverts would like to do when they don’t feel like being social. For this emoji, I didn’t really sought out for a certain inspiration other than by looking at the [emojis I have in my phone](https://emojipedia.org/samsung/) to look at their structure and find a similar mood I had in mind, which was neither happy nor sad. 

For this exercise I used a lot of the basic shapes, like ellipse and rectangle (rect), and then coordinated the right strokes and stroke weight, and I also used bazier to connect the headphones together. For the two eyes I used ellipseMode to get make it easier to coordinate them correctly on the head. When I had made my smiley the hard part followed because I had a vision on making the smiley become transparent or clear to get a sense of the emoji disappearing or becoming invisible, in a cartoonish way. This got my in a period where I tried a couple of different things like different mouseIsPressed functions, but I wasn’t satisfied with that function, as it disapproved with my original vision for the project. In the end I played around with the slide function and found a way to make the colours fade and change and that become my outcome with the introvert emoji.


[MiniX2 (mundbindemoji)](https://andreayrm.gitlab.io/aesthetic-programming/MiniX2)    

[See code to MiniX2](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX2/sketch.js)

![Screenshot](mundbindemoji.png)

2.	The other emoji, I made, was a ‘Husk mundbind’ emoji, similar to the ones the danish health authorities have issued to the public during the pandemic. These are signs that we are seeing in our daily life’s so I felt it was relevant to make an emoji that supports the rules that we, as a society, must follow to get a better outcome for the future and because it has become our new normal. By that said, I did a small research to see if there was such thing as a mask emoji and found out that there is a smiley emoji with a mask but no emoji that consists of only the mask. For that reason I also wanted to make my emoji more cultural for Denmark to see have a emoji that closely resembles [the sign](https://www.sst.dk/da/udgivelser/2020/plakat-her_bruger_vi_mundbind) from the Danish health authorities.

The syntax used for this exercise where a mixture of different shapes like rect, bazier and lines. I also used the text syntax for the first time and felt that it was pretty basic experience, in addition, I felt like I needed to have the background to have a framerate(15) to ‘zhoosh’ it up a little because this particular emoji is a bit basic. 

**Reflection:**

I do like my two designs but I like the Introvert emoji more because I feel like I had a vision from the start of what I wanted to do and I feel like I have achieve that. The mask emoji is a bit simple and there for it doesn’t do much for my, but I feel like the final outcome is visually appealing so I can check in that box. All in all, I didn’t go very political for this exercise but I feel like I steered in a more cultural approach to the assignment and I am happy with my outcome and with my learning process so far, as I have learned a lot with this exercise, specially with the part I struggled with as I got to know the mouse function better and exercised a bit with variables. 
