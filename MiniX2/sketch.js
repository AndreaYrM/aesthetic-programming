function setup() {
  createCanvas(400, 400);
  frameRate(15);
}

function draw() {
  background(random(230, 250));
  fill('rgb(3, 51, 99)');
  stroke(0);
  strokeWeight(6);
  ellipse(200, 200, 350);
  //Center mundbind
  stroke(255);
  strokeWeight(4);
  rect(115, 170, 170, 90, 10);
  //left
  stroke(255);
  noFill();
  strokeWeight(1);
  bezier(115, 190, 40, 175, 50, 220, 115, 240);
  //Right
  stroke(255);
  noFill();
  strokeWeight(1);
  bezier(285, 190, 380, 175, 310, 220, 285, 240);
  //Line inside of the mask
  line(135, 230, 260, 230);
  line(135, 200, 260, 200);
  //'Husk mundbind' text 
  textSize(32);
  fill(0, 102, 153, 51);
  text('Husk mundbind', 85, 130);
}
