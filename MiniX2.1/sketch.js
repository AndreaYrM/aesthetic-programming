let slider;
function setup() {
  createCanvas(500, 500);
    colorMode(HSB);
  slider = createSlider(0, 360, 0, 40);
  slider.position(210, 450);
  slider.style('width', '80px');
}

function draw() {
    let val = slider.value();
  background(153, 255, 204);
  //draw a light green ellipse in size 270
  fill(val, 100, 100, 1);
    colorMode(RGB, 225, 204, 153, 2);
   ellipse(250, 250, 190, 190);
  //draw one eye
  ellipseMode(RADIUS);
  fill('grey');
  ellipse(180, 200, 20, 40);
  //draw the other eye
  ellipseMode(RADIUS);
  fill('grey');
  ellipse(300, 200, 20, 40);
  //draw mouth
  rect(170, 310, 150, 35, 20);
  //draw the headphone muffs
  rect(20, 160, 45, 157, 20, 5, 5, 20);
  rect(435, 160, 45, 157, 5, 20, 20, 5);
  //draw headphone piece
  noFill();
stroke('grey');
strokeWeight(8);
bezier(460, 160, 440, 5, 30, 10, 40, 160);
}
