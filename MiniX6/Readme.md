[Click here to see my program](https://andreayrm.gitlab.io/aesthetic-programming/MiniX6/)

[Click here to see my code](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX6/sketch.js)

![Screenshot](MiniX6/screenshot.6.png)

[Revisit my MiniX1](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX1/Readme.md)

**Revisit my past:** For this week I took a look at my first miniX and tried to add some syntaxes that I had learned for the last 6 weeks. The reason that I chose this particular miniX to revisit was mainly just because I felt like it would be fun to reflect on where I was in my coding experience, only a couple of weeks ago, and to take this simple sketch that I had made and add some more to it.
For my first miniX I had only played around with some coordinates and shapes, so I stayed true to the original sketch by keeping those simple shapes of triangles and ellipses, but added another triangle, opposite to the original one, and made a four loop function of ellipses going across the screen diagonally. This particular for loop became difficult to manage but I found a code, in the [p5.js editorial library](https://editor.p5js.org/cs4all/sketches/ry6AMxmK7), that I copied to make this work. Despite this being difficult task, I found it helpful to practice my for loops. 

For the two triangles, I made a if statement to make them switch colours and added some text to make the aesthetics of this ‘Stop’ and ‘Go’ notion become more obvious. I also made the colours of all the shapes have a random function of colours to give a flickering feeling to it, mainly for the purpose to instore a bit panic in this notion of always having to ‘Stop’ and ‘Go’.

When I first looked at the code to my first miniX, I found it interesting to see that in my original sketch I had used a mouse function of some sort, that was non functional and did not do anything to the sketch. Seeing this I saw a clear difference in how I have become familiar to these syntaxes, but though, I cant really compare myself to my first experience with coding as it is rather obvious that there has been a great deal of code learning for the past couple of weeks. With that said I would maybe want to rethink my choice of miniX to revisit if I had to do another one of these kinds of exercise in the future.

**Critical Making:** As our world keeps evolving towards an active digital and computational function of living, we need to learn, and then remain, critical to the different software, and hardware, that are constantly being presented to us. 

> "critical making is about turning the relationship between technology and society from a 'matter of fact' into a matter of concern'" (Ratto, p. 20). 

The reason that critical making is important is to get a better understanding on the linkage between our culture and the ever-evolving technological development. This can be done through different methods, like for example through aesthetic programming.

**Aesthetic Programming:** During this week I found it to by rather helpful to reflect on the semester, so far, and look at how the course has been structured and if it has helped my understanding on the subject of aesthetic programming. When coming into this course I wanted to get a better insight on what is happening behind the interface and I didn’t really think we would go as much into the conceptual part of it. Personally, I like the conceptual part a bit more than the coding part, but I really like the way we approach the miniX, each week, and I am still learning how to have a nice balance between the conceptual part and the coding part, so they intertwine with each other. 

A commonly misunderstood word is the word aesthetic, as it does not necessarily mean that something is beautiful but rather what that thing represents. Action appearance and behaviour of something can play a key role in how that something can be represented, in a meaningful way, to the world. While programming is not only used to make profit, by making an app or a website, but also to express our self aesthetically though code, and aesthetic programming has given us the opportunity to do so while getting a clearer understanding on the computational culture. 

As designer we can have an influence in this digital age by not only learning how to code but also to understand the code in its whole and what in fact is happening behind the interface. By becoming aware about how the some corberations see users, how our data are being handled, how to become aware of what is hidden from us and what is an open source for the public, and of course, also;
 > “produce immanent critique drawing upon computer science, art, and cultural theory."(Soon, p. 15).


