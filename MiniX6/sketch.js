var tr1;
var tr2;

function setup() {
  createCanvas(600, 400);
  noFill();
  stroke(255, 130, 0);
  tr1 = triangle(20, 380, 20, 30, 520, 380);

  tr2 = triangle(580, 20, 580, 360, 80, 20);
}

function draw() {
  background(255, 100, 0);

//Make the for loops ellipses dialoginal across the screen
  fill(random(200, 255), 100, 0);
  var num = 12; //number of ellispes on the screen

  let x = 0;
  let y = 0;

  for(var i = 0; i <= num; i++) {
    ellipse(x, y, 15, 15);
    //make them diagoncal
    x += width/num;
    y += height/num;
  }

//If statements for the colored triangles + texts
  if (keyCode==32){
    fill(0, random(200, 255), 0);
    tr1 = triangle(20, 380, 20, 30, 520, 380);
    fill(255, 100, 0);
    text('GO', 200, 360);
    textSize(22);
    fill(255);
    text('Press Enter', 400, 100);
  }else if (keyCode==13){
    fill(random(200, 255), 0, 0);
    tr2 = triangle(580, 20, 580, 360, 80, 20);
    fill(255, 100, 0);
    text('STOP', 300, 80);
    textSize(22);
    fill(255);
    text('Press Space', 100, 300);
  }else{
    textSize(22);
    fill(255);
    text('Press Space', 100, 300);
    textSize(22);
    fill(255);
    text('Press Enter', 400, 100);
  }
}
