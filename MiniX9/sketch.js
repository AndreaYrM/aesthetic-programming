var api = 'https://api.giphy.com/v1/gifs/search?';
var apiKey = '&api_key=dc6zaTOxFJmzC';
var queryhands = '&q=clap+hands';
var queryfeet = '&q=stomp+feet';
var queryAp = '&q=coding';
var buttonHands;
var buttonFeet;
var buttonAp;
let myFont;

function preload() {
  myFont = loadFont('Pumpkin.otf')
}

function setup() {
  createCanvas(1500, 800);
  background('Yellow');
  textFont(myFont);
  textSize(25);
  fill('orange');
  textAlign(CENTER);
  text('If your happy and you know it...', width/2, 80);

  //Create 'Clap your hands' button
  buttonHands = createButton('Clap your hands');
  buttonHands.style('background-color', 'grey');
  buttonHands.style('padding', "10px 14px");
  buttonHands.style('font-size', '20px');
  buttonHands.position(width/2, 120);
  buttonHands.mousePressed(giphyHands);

  //Create 'Stomp your feet' button
  buttonFeet = createButton('Stomp your feet');
  buttonFeet.style('background-color', 'grey');
  buttonFeet.style('padding', "10px 14px");
  buttonFeet.style('font-size', '20px');
  buttonFeet.position(width/2, 180);
  buttonFeet.mousePressed(giphyFeet);

  //Create 'Do some coding' button
  buttonAp = createButton('Do some coding');
  buttonAp.style('background-color', 'grey');
  buttonAp.style('padding', "10px 14px");
  buttonAp.style('font-size', '20px');
  buttonAp.position(width/2, 240);
  buttonAp.mousePressed(giphyAp);

}

function giphyHands() { //Load the clapping hands gifs
  urlHands = api + apiKey + queryhands;
  loadJSON(urlHands, gotHandsData);

}

function giphyFeet() { //load the stomping feet gifs
  urlFeet = api + apiKey + queryfeet;
  loadJSON(urlFeet, gotFeetData);
}

function giphyAp() { //load the coding gifs
  urlAp = api + apiKey + queryAp;
  loadJSON(urlAp, gotApData);
}

function gotHandsData(handsGiphy) { //Make random gifs appear, within the JSON file
  i = floor(random(handsGiphy.data.length));
  gif1 = createImg(handsGiphy.data[i].images.original.url); // path to choose the url of the gif
  gif1.position(width/2-300, 300);
  gif1.size(650, 500);
}

function gotFeetData(feetgiphy) {
    i = floor(random(feetgiphy.data.length));
  gif1 = createImg(feetgiphy.data[i].images.original.url);
  gif1.position(width/2-300, 300);
  gif1.size(650, 500);
}

function gotApData(Apgiphy) {
    i = floor(random(Apgiphy.data.length));
  gif1 = createImg(Apgiphy.data[i].images.original.url);
  gif1.position(width/2-300, 300);
  gif1.size(650, 500);
}
