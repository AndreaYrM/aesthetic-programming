[RUNME miniX1](https://andreayrm.gitlab.io/aesthetic-programming/MiniX1/)

[See my code](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX1/sketch.js)


![Screenshot](MiniX1-screenshot.png)

The first mini exercise had the purpose of getting us to play around with the program and to familiarise us with coding. I have no experience with coding or programming in general, so I took a couple of baby steps for this exercise by starting with the colour range. After that I played around with lines and made a triangle. I was aware of the possibility of just coding a simple triangle, but I wanted to exercise and get the feel of the coordinate, so I came up with this right scalene triangle with a light orange stroke. 

The making of the triangle was a nice little project to get started with, so following that I played around with making a [square with round corners](https://p5js.org/reference/#/p5/square), again, just to play around with the coordinates and to test out shapes, but I deleted that later on as I didn’t find it visually appealing to the project. I later added the [get-started sketch to my project](https://p5js.org/get-started/)just to see if I could use that in some way for making my project come alive, but again deleted that when I had played around with it a bit. 

After that came just a long period of my just trying different things out. Following that,  I started to  see a pattern of problems with my add-ons always coupling with the stroke/fill of my line triangle shape so I felt I could not figure out how to have the triangle separate colour from, for example, the square. Either way I just had fun with the program and ended with trying out circles and, again, figuring out the coordinate so they looked good with my triangle, and then added a press function to brighten up my project. All in all, I ended up with a simple and minimalistic look, mostly because I this was my first time with programming, and I had spent very much time playing around but I still didn’t want it too to look to chaotic. 

I feel like I have had an okey time with my first coding experience. It was all very overwhelming, and I did often get the feeling of not recognising how to solve the problem I had, but because of my lack of experience with programming, I kind of just brushed the problem away and tried to go in another direction, so I wouldn’t get stuck for too long. So, my main goal was to explore the program and try different things out. Like I have explained in my coding process, I liked to play around with the coordination a bit, to get the feeling of the canvas, but I also wanted to play around with lines because of an inspiration I got when exploring ideas on a Facebook site for [Creative Coding](https://www.facebook.com/groups/creativecodingp5) where there was a project where a string of vertical lines moved slowly up and down. 

I feel like next time I would like to draw my idea up first before I start coding, to get a clearer sense on what I want to accomplish and so I can get better in the problem solving part of it.

The coding process, for me, felt a lot like writing and reading in the sense of having to get the general idea of the coding literacy and by trying this out for the first time I understand, in a way, that there is a big vocabulary for programming and it takes time to get the hang of it, just like reading and writing.

Coding and programming mean problem solving for me, and a great understanding on what is happening behind the interface. By reading the weekly text, I feel like there are some ideas from Annette’s Vee book, on how programming is essential tool for the future, that I have been hearing people talk about for some time. By analysing the idea, I got a greater sense on the programmer’s responsibility and job. In addition, I by comparing programming literacy to reading and writing literacy got my looking at the programming experience with a different view. 
