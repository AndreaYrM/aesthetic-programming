For this week's MiniX, I have collaberated with [Julie Fey](https://gitlab.com/Julie_Fey/aesthetic-programming/-/tree/master/MiniX8)

[Click here to see our project](https://andreayrm.gitlab.io/aesthetic-programming/MiniX8/)

[Click here to see the code](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX8/sketch.js)

![Screenshot](MiniX8/screenshot.png)

**Description:** Our project is called “Glimpses Of Time” and consists of a program that continuously generates a poem. It is written from first singular person's view . The poem starts by declaring the time and by describing how it looks outside. The first person then addresses an unknown person, who returns a statement. The first person then describes what they can see and how they feel.
The poem is made to take a snapshot of a single moment in time and illustrate the temporality of moments, feelings and life. It describes one such moment briefly through each of the sentences, and then it is gone again, almost before you can read it. It is a poem about time and the fragility of life, and also about the appreciation of each of the small moments. Time (and life) is brief and all we have is the present moment, this very second.

**Technical:** The poem is made up of five parts (each line of the poem), which is further split into two parts. The two parts consist of a constant piece of text, and a generative piece of text. These parts are all then put together, which creates a poem of five sentences, whereas some part of each of the sentences are constantly changing. We have called the constant pieces of text for constantText() and the generative pieces for randomText(). The random pieces of text are derived from json files.

**constantText():** These pieces of text never change. They serve as the beginning of each sentence, and they are pretty neutral and almost work as “prompts” for what kind of words there could be generated.

**randomText():** This part is where the json files come in. All the words here are randomly selected from a specified json file. We have five json files total. Each of them are constructed in the same way; they have a description, which doesn’t serve any purpose other than to help the programmer see what the file contains. Then they each have an array. In the array we have put different words that we felt could fit into the given part of the poem.
Back in the main code, we retrieve the date from the json files in the randomText() function. First we make sure to load the json files in the preload() function, where we define a variable for each of the loaded files. This variable is used to define the individual json file. We then need to access the array in the json file, which we do by using the dot notation (example: time.timestamps - time being the variable used to define the json file and timestamps being the name of the array). We also wanted to choose a random member of the array, so we included the random function. This means that the code that pulls the data from the json file ended up looking like this:
text(random(time.timeStamps), x, y);

Furthermore, we decided to use a [handwriting font](https://fontlibrary.org/en/font/vshandprinted) so we made sure to also load this in the preload() function. As for the json files, two of the five we downloaded from [here](https://github.com/dariusk/corpora/blob/master/data/words/literature/shakespeare_phrases.json) and [here](https://github.com/dariusk/corpora/blob/master/data/humans/moods.json). The three remaining files we wrote ourselves.

**Vocable code - analysis:** Like it has been discussed before, software is constructed from language and in programming, coding often acts as a script, command, or even instruction, in order to communicate with the computer. When looking at how the code serves as a form of speech, then, according to Cox et al., _“it can also be said to be like poetry inasmuch as it involves both written and spoken forms.”_ (Vocable code, p. 17)

Scholars and artists continue to explore these connections between speaking and coding for the purpose of using programming as more than an aesthetic tool for producing various forms, but rather to _“explore the material connections and creative tensions between the two"_ (Soon, Winnie. page 167)

This consideration of how coding can “voice” wider political issues has been focused on in a particular software artwork, by Winnie Soon, called Vocable code, which we have been working with closely during our Tuesday lecture. This particular “codework” both showed the syntaxes displayed in a vocal way, giving us a behind the scenes pass to understand the deeper meaning of the code, as well as the code simultaneously operates in a poetic way with collective voices stating, in their opinion, what “Queer is”.

In the required text by Geoff and McLean about vocable code, they explain that: _“Like poetry, the aesthetic value of code lies in its execution, not simply its written form.”_ (Cox et al. page 1)

Meaning that, the aesthetic of the code can serve the purpose of showing the deeper meaning of the code that simply what is actually written. For our program, it can be interpreted in the way the temporality plays a key role for the program as well as how our code gives the overall structure an understandable concept to it’s execution, which one could argue, make the code look more like a language.

**Reference:**

Cox, Geoff, Alex McLean, and Adrian Ward. "The aesthetics of generative code." Proc. of Generative Art. 2001.

Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press, 2013), 17-38.

Soon, Winnie, and Geoff Cox. "Aesthetic programming: A handbook of software studies." (2020): 167.
