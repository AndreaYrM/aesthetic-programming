[Program](https://andreayrm.gitlab.io/aesthetic-programming/MiniX5/)

[Repository](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX5/sketch.js)

![Screenshot](MiniX5/screenshot.png)

For the first time, in this semester, I actually did not have a specific idea of what I wanted do, or in which direction to go. All I knew was that I wanted to develop a code, which would fulfil the basic task of the exercise, and I wanted my code to be rather aesthetically appealing, similar to the generative artwork which I looked at online. 


**My Program:** The program I made, shows a canvas with rectangles appearing randomly all over the canvas, in random size. Every five seconds, a set of ellipses start to appear, in random places and in random sizes, and then fade away in the background, giving a new appearance to the “mash” of rectangles. My generative program has two set of rules:
1. 	When the frame flickers, a set of rectangles appear in random places, and change in placement for each frame count.
2.	For every five-frame count, a randomly placed set of ellipses appear and then fade away.

The purpose of the framerate function for this sketch is to form a visualisation of the patterns that the shapes form over time and is constantly changing. The random function also plays a certain role for the sketch, as it allows the shapes to create a range of colour scheme to the canvas. It also makes the placement of both shapes become random and gives the power away from the programmer and to the computer which can generate some interesting things. The for loop gives the program the effect of being able to experience different patterns every second passing. In addition, I would like to add that I had a look at an [example code](https://editor.p5js.org/allison.parrish/sketches/H1__vQxiW) from the p5.js open source editor library, where time and colour changing was the main focus.


**Randomness and rules:** These two words can be seen as opposite, but still they worked great together for my generative program and have a great role in computational culture. The rules I used produced a set of framework for what actions I wanted to preform, in each step of the programs running process. It gave the computer a humanistic guideline to follow which I, the dominate programmer, chose for the look and feel of the program.


**Auto-generator:** This evolving state of the program, can open up for some interesting visual experience, which becomes clear when working with rules and random functions as they become a great foundation for the generative program. When we think of random we often understand it to be something out of our reach, even in other mini exercises that we have done, we think of this function to be something that we don’t need to think about, the computer takes care of this, like it is shuffling cards. This week we were introduced to the term pseudo randomness, from the chapter ‘Randomness’ 10 PRINT CHR$(205.5+RND(1)); : GOTO 10” by Nick Montford, which opens up the discussion about how something can appear random, but isn’t, there is in fact an algorithmic rule behind this random function. We can also see an example of this in nature as it seems like branches on trees form a random growth of directions, length and size, to create a natural artwork. But in reality, the tree does not grow its branches in a random function, it depends on various things like where the sun shines on the tree or how the wind effects the branches. 
With that said, this so-called random element has given great meaning to generative artwork, from  philosopher and artist, John Cage’s point of view, 

> “random elements remove individual bias from creation; they may be used to reach beyond the limitations of taste and bias through “chance operations.””(Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 126.)

By removing a small part of individualism from the generative program, the programmer can create something that even he can’t fully visualises until the code evolves self over time, and from there the programmer can chose to tinker more to the coding, or she can let it be for others to enjoy.

Before reading the require reading, I had not really given the subject a thought, but by reflecting on this topic and doing the exercise, I feel like it can have great value on how to approach the future exercises and also gives a new perception on the computational thought. 
