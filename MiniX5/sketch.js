let circleColor = 0
let squareColor = 0

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(1);
  circleColor = random(255);
  squareColor = random(255);
}

function draw() {
  background(100, 110);
  stroke(circleColor);
  strokeWeight(2);
  noFill();
  for(var x = 0; x < width; x = x+10){
    if(frameCount % 10 == 5){
      circleColor = color(random(255), 200, random(200, 255));
      stroke(circleColor);
      ellipse(random(windowWidth), random(windowHeight), random(2, 50));
    } else {
      squareColor = color(random(200, 255), random(200, 255), random(200, 255));
      stroke(squareColor);
      rect(random(windowWidth), random(windowHeight), random(10, 50), random(10, 50));
    }
  }
}
