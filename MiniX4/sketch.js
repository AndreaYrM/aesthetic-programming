let buttonl; //the left wide button
let buttonr; //the right side button
//let mic; // the microphone
let capture; //the camera capture
let ctracker; //tracking the face
let input; //input button
let subutton; // submit button
let statement; // for the input box

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(100);

  //audio
  //mic = new p5.AudioIn(); //I'm going to use the AudioIn - from the p5.js library
  //mic.start(); //activate the mic

  //video
  capture = createCapture(VIDEO); //using the camera to capture
  capture.size(620, 480); //the canvas size - is the actual size of the webcam canvas
  capture.position(0,0); //position of the webcam inside the canvas

  //face ctracker
  ctracker = new clm.tracker(); //identifying the usage of the tracker library
  ctracker.init(pModel); //the particular model from the library
  ctracker.start(capture.elt); // start doing the capture

  //creation of the left button
  buttonl = createButton('X');
  buttonl.style('font-size', '20px');
  buttonl.style('font-family', 'arial');
  buttonl.position(100, 100);

  //styling of the left button
  buttonl.style("strokeWeight", "5");
  buttonl.style("color", "black");
  buttonl.size(30, 40);

  //creation of the right button
  buttonr = createButton('X');
  buttonr.style('font-size', '20px');
  buttonr.style('font-family', 'arial');
  buttonr.position(200, 200);

  //styling of the right side button
  buttonr.style("strokeWeight", "5");
  buttonr.style("color", "black");
  buttonr.size(30, 40);

  //mouse capture
  buttonl.mousePressed(change);
  buttonl.mouseOut(revertStyle);

  input = createInput();
  input.position(20, 410);

  subutton = createButton('submit')
  subutton.position(140, 410);
  subutton.mousePressed(info);

let col = color('green'); //let the text become more visual with green background
  statement = createElement('h2', 'Please submit your info');
  statement.position(20, 360);
  statement.style('background-color', col);
}

function draw() {
  //draw the captured video on a screen with image filter
  image(capture, 0, 0, 640, 480);

  let positions  = ctracker.getCurrentPosition(); //Gets the 70 tracker points (form library)
  //check the availability of web cam tracker
  if (positions.length) {
  buttonl.position(positions[32][0]-10, positions[32][1]-10);
  buttonr.position(positions[27][0]-10, positions[27][1]-10);
    }
  }

  //the info function - to make what is written in the input appear on the canvas
  function info() {
    const info = input.value();

    for (let i = 0; i < 50; i++) {

      push();
      translate(random(width), random(height));
      fill(0,80);
      textSize(50);
      textFont('Georgia'); //CSS styling
      text(info, 0, 0);
      pop();

    }
  }

  function change() { //mousePressed
    buttonl.style('background', 'grey');
    buttonr.style('background', 'grey');
  }

  function revertStyle() { //mouseOut
    buttonl.style('background', 'white');
    buttonr.style('background', 'white');
  }

  function keyPressed() {
    if(keyCode ===32) {
      buttonl.size(50, 50);
      buttonr.size(50, 50);
      buttonl.style("background", "red");
      buttonr.style("background", "red");
      buttonl.style('font-size', '40px');
      buttonr.style('font-size', '40px');
    } else {
      buttonl.size(30, 40);
      buttonr.size(30, 40);
      buttonl.style("background", "red");
      buttonr.style("background", "red");
      buttonl.style('font-size', '20px');
      buttonr.style('font-size', '20px');
    }
  }
