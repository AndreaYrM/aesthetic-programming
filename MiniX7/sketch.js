let bubbles = []; // the class name
let bubblesPopped = 0; // variable to keep track of popped bubbles
let score = 0; // variable to keep track of score
let myFont;

function preload() { // Use a specific font
  myFont = loadFont('Pacifico.ttf');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  textFont(myFont);

  for (let i = 0; i < 30; i++) { //for loop to create new buuble
    bubbles.push(new Bubble());
  }
}

function draw() {
  background('#c8c3cc');

  for (let bubble of bubbles) { // "bubble": a reference to the elements in the array
    bubble.move();
    bubble.show();
}
// Create a text for scoreboard
textSize(40);
noStroke();
fill('#563f46');
text("Score: " + score, 40, 60);

// Create a text for instructions
textSize(35);
noStroke();
fill('#563f46')
text("POP the bubbles ;* ", width/2, height/2);
}

function mousePressed() {
  for (let bubble of bubbles) { //same "for loop" as used earlier
    let d = dist(bubble.x, bubble.y, mouseX, mouseY); //identify the location insode each random sized bubble
    if (d < bubble.size) { //if mouse is located inside an bubble...
      bubble.c = '#c8c3cc'; //change the colour (to become same as background)
      score += 1; //then add score for each click
    }
  }
}
// Create Bubble class
class Bubble {
  constructor() {
    this.x = random(width);
    this.y = height, random(width);
    this.xspeed = random(-1, 3);
    this.yspeed = random(0, 3);
    this.size = random(40, 100);
    this.c = '#f0efef';
    this.pos = new createVector(height, random(width));

  }

  move() {
    // Have the bubbles move in a certain way - "float" upwards
    this.y -= this.xspeed;
  }

  show() {
    noStroke();
    fill(this.c);
    ellipse(this.x, this.y, this.size, this.size);
  }
}
