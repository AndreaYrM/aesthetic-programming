[Click here to see my program](https://andreayrm.gitlab.io/aesthetic-programming/MiniX7/)

[Click here to see my code](https://gitlab.com/AndreaYrM/aesthetic-programming/-/blob/master/MiniX7/sketch.js)

![Screenshot](MiniX7/screenshot.7.png)

**My game and how it works:** My game is a bubble popping game which contains the simple task of “popping” the bubbles on the screen, before they leave the canvas. The bubbles appear at the bottom of the canvas, in random places across the width of the canvas, and then “float” upwards to reach the height of the canvas, as they then disappear. In the top corner is a scoreboard, which keeps track of how many times a bubble gets popped. 

**The code:** For this exercise I had a lot of problems and I can honestly say that I have never spend as much time with any miniX over this semester, as I have with this one. First, I tried tinkering with the code from Monday’s class, by making few simple changes to the position for the class constructer of the tofu and then changing the object, to show a no fill ellipse. These small changes to the code gave a whole new look to the sketch, displaying bubbles floating upward to the screen, which made me really wanting to add some interaction to the bubbles with the `mousePressed` function. But with that followed some long and frustrating time with the code, because I could not get the mouse to interact within each bubble successfully. I connected this problem early on with the radius(`this.r`) of the bubbles not being correctly portrayed in the code, so  I watched a lot of videos, did some research, and then finally sought help from the instructors. When everything had failed, I tried to start coding from scratch, which led me to understand my problem, as it lied in the `mousePressed` function, not in the Bubble class. Long story short, I have been knees deep in this code and have managed to produce a simple looking game, which actually is build on some ‘lies’, for the scoreboard doesn’t really have a great purpose, as the game doesn’t actually end at any time. Also, the bubbles do not ‘pop’, they will just become the same colour as the background, which makes them look invisible. 
All in all, I don’t really feel like the final product has conceived my expectations on how I wanted my game to look, but I still feel like this was a great week for my learning experience in coding, so, I’m thankful for that. 

**OOP & abstraction:** OOP defines a programming technique which has the programmer thinking of a particular code as sectional, with its own set of attributes, behaviour, functions and more. This is a good method to be used when designing a larger set of code, for example, when making a game, where a certain blueprint(class) has its own description and a behavioural mode to go by. An example of this could be found in a popular pc game called Sims, where you can make multiple version of a person (or in some version, an animal) and you can choose how that particular person acts or behaves, during the game. But even tough, the choices can seem endless for these digital personas, they are still a superficial interpretation of human beings, as we are so much more complex and diverse than these computer-based persons, and one could argue that there won’t ever become a time where an identical  programmed version of myself will exist in the world.

In my program I have taken something from the outside world – bubbles - as an inspiration for making a game where we take these well-known, and satisfying, gesture of popping a bubble. This particular bubble was given different kinds of attributes, which describes the class to become this bubble, as we know them to look like. As important, it was also given some set of rules, on how it should behave for this particular sketch, again, to have a certain feel to how it behaves in the real world. As explained in an essay on object orientation:


> “..certain kind of unknowability is produced through the technically facilitated processes of abstraction.”(Fuller, M. & Goffey, A.)

We can look at the way things have been created in the world with object orientation abstraction, by taking something we know to make a relation to the things we know and complex computational things we might not understand. 

**Reference:**

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in Matthew Fuller, How to be a Geek: Essays on the Culture of Software (Cambridge: Polity, 2017).
