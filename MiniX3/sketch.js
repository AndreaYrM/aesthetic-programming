let xPos = [1, 2, 3, 4, 5];
let yPos = [1, 2, 3, 4, 5];
let xCtr = 0;
let yCtr = 0;
let speed;
let currentMillis;

function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(10);
}

function draw() {
  background(70, 80);
  drawElement();
}

function drawElement() {
let num = 10;
//Make the first/orange throbber
  push();
   translate(xPos[xCtr] + 100, yPos[yCtr] + 100);
    let cir = 360/num*(frameCount%num);
    rotate(radians(cir));
       timer = currentMillis + speed;
       textSize(12);
       fill(random(155, 255), 100, 0); //fill the text with random colours, rappidly changing.
       text('Loading....', 15, 0);//'Loading...'text and size of the 'circle'
  pop();

//Make the second/green throbber
  push();
   translate(xPos[xCtr] + 100, yPos[yCtr] + 270);
    let cir2 = 360/num*(frameCount%num);
    rotate(radians(cir));
       timer = currentMillis + speed;
       textSize(12);
       fill(100, random(155, 255), 0); //fill the text with random colours, rappidly changing.
       text('Loading....', 15, 0);//'Loading...'text and size of the 'circle'
  pop();

//Make the third/blue throbber
  push();
   translate(xPos[xCtr] + 100, yPos[yCtr] + 450);
    let cir3 = 360/num*(frameCount%num);
    rotate(radians(cir));
       timer = currentMillis + speed;
       textSize(12);
       fill(0, 100, random (155, 255)); //fill the text with random colours, rappidly changing.
       text('Loading....', 15, 0);//'Loading...'text and size of the 'circle'
  pop();

  //input first text
    textSize(32);
    fill(random(200, 255), 100, 0);
    text('Loading in process...', 230, 110);

  //input second text
    textSize(32);
    fill(100, random(200, 255), 0);
    text('Wait a little longer..', 230, 300);

  //input third text
    textSize(32);
    fill(0, 100, random(200, 255));
    text('Just wait, Ok!', 230, 460);
    textSize(22);
    fill(0, 100, random(200, 255));
    text('Its going to happen eventually!', 233, 485)
  }
function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
